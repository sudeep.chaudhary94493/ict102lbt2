package ict102.lbt2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Write a program that removes duplicates from an array.

//Output:
/*
arr: [3, 6, 2, 2, 43, 7, 8, 5, 3, 44, 90, 34, 2, 2, 56, 7, 56, 8, 3, 4, 56, 66, 2, 43, 7, 8, 5, 3]
list: [3, 6, 2, 43, 7, 8, 5, 44, 90, 34, 56, 4, 66]

*/

public class STD_12001335 {
	public static void main(String[] args) {
		int[] arr = { 3, 6, 2, 2, 43, 7, 8, 5, 3, 44, 90, 34, 2, 2, 56, 7, 56, 8, 3, 4, 56, 66, 2, 43, 7, 8, 5, 3 };
		List<Integer> list = new ArrayList<>();

		// your code here

		System.out.println("arr: " + Arrays.toString(arr));
		System.out.println("list: " + list);
	}
}
