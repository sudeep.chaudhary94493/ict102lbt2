package ict102.lbt2;

//Write a program that displays numbers user entered in the order entered with their indexes.
//Use a Java array to store the numbers.
//Use java.util.Scanner for user input.

//Output:
/*
Enter a number: 10 (<-this is user input)
Enter a number: 4 (<-this is user input)
Enter a number: 6 (<-this is user input)
Enter a number: 8 (<-this is user input)
Enter a number: 2 (<-this is user input)


=== Result ===
1: 10
2: 4
3: 6
4: 8
5: 2

*/

public class STD_12001352 {
	public static void main(String[] args) {

	}
}
