package ict102.lbt2;

//2D Array: Write a program that results the given output.
//Use loop statement.

//Output:
/*
2 4 6 8 10 12 14 16 18 
3 6 9 12 15 18 21 24 27 
4 8 12 16 20 24 28 32 36 
5 10 15 20 25 30 35 40 45 
6 12 18 24 30 36 42 48 54 
7 14 21 28 35 42 49 56 63 
8 16 24 32 40 48 56 64 72 
9 18 27 36 45 54 63 72 81 

*/

public class STD_12001334 {
	public static void main(String[] args) {
		int[][] arr = new int[8][9];

		// fill the array's values here.

		// display the array
		for (int i = 0; i < arr.length; i++) {
			for (int j = 0; j < arr[i].length; j++) {
				System.out.print(arr[i][j]);
				System.out.print(" ");
			}
			System.out.println();
		}

	}
}
