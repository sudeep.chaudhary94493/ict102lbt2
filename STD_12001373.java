package ict102.lbt2;

//Write a program that checks a password.
//The password should include at least 10 charactors.
//The password should only be composed to alphabet letters and digits.

//Output:
/*
#abcd345abcdgde45 is not valid
abc12 is not valid
a23bcd123abc is valid
123bcd123abc is valid
a23bcd_vdkeder34 is not valid
*/

public class STD_12001373 {
	public static void main(String[] args) {
		String pwd1 = "#abcd345abcdgde45";
		String pwd2 = "abc12";
		String pwd3 = "a23bcd123abc";
		String pwd4 = "123bcd123abc";
		String pwd5 = "a23bcd_vdkeder34";

		System.out.println(pwd1 + " is " + (isValidate(pwd1) ? "" : "not ") + "valid");
		System.out.println(pwd2 + " is " + (isValidate(pwd2) ? "" : "not ") + "valid");
		System.out.println(pwd3 + " is " + (isValidate(pwd3) ? "" : "not ") + "valid");
		System.out.println(pwd4 + " is " + (isValidate(pwd4) ? "" : "not ") + "valid");
		System.out.println(pwd5 + " is " + (isValidate(pwd5) ? "" : "not ") + "valid");
	}

	private static boolean isValidate(String p) {
		// modyfy this method.
		return false;
	}
}
