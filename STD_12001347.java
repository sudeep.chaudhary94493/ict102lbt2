package ict102.lbt2;

import java.util.Arrays;

//Write a program that compares two items form parallel arrays.
//The result array stores who has bigger number at each index.
//The result array stores "Draw" when the items were same.

//Output:
/*
Palyer1: [3, 6, 76, 3, 5, 782, 312, 5, 7, 8, 45, 2, 3, 5, 7, 6, 8, 2, 3, 4, 2]
Palyer2: [33, 8, 73, 3, 5, 72, 314, 8, 34, 8, 23, 22, 3, 25, 5, 66, 8, 2, 3, 4, 2]
Result: [Palyer2, Palyer2, Palyer1, Draw, Draw, Palyer1, Palyer2, Palyer2, Palyer2, Draw, Palyer1, Palyer2, Draw, Palyer2, Palyer1, Palyer2, Draw, Draw, Draw, Draw, Draw]

*/

public class STD_12001347 {
	public static void main(String[] args) {
		int[] palyer1 = { 3, 6, 76, 3, 5, 782, 312, 5, 7, 8, 45, 2, 3, 5, 7, 6, 8, 2, 3, 4, 2 };
		int[] palyer2 = { 33, 8, 73, 3, 5, 72, 314, 8, 34, 8, 23, 22, 3, 25, 5, 66, 8, 2, 3, 4, 2 };
		String[] arr_result = new String[palyer1.length];

		// your code here

		System.out.println("Palyer1: " + Arrays.toString(palyer1));
		System.out.println("Palyer2: " + Arrays.toString(palyer2));
		System.out.println("Result: " + Arrays.toString(arr_result));
	}
}
