package ict102.lbt2;

//Write a program that results the given output.

//Output:
/*
Budget: 10000.0, Cost each student: 280.5
Total Cost for 27 Students (Throw away less than $1): 7573
Remaining budget: 2427

*/

public class STD_12000960 {
	public static void main(String[] args) {

		double budget = 10000.00;
		double costEach = 280.50;
		int numberStudents = 27;

		System.out.println("Budget: " + budget + ", Cost each student: " + costEach);
		System.out.println("Total Cost for " + numberStudents + " Students (Throw away less than $1): ");
		System.out.println("Remaining budget: ");
	}
}
