package ict102.lbt2;

//Write a program that checks a password.
//The password should start with an alphabet letter or a digit.

//Output:
/*
#abcd345abcdgde45 is not valid
$#%$&(@_#@#$#$# is not valid
a23bcd is valid
123bcd is valid
a23bcd_vdkeder34 is valid

*/

public class STD_12001353 {
	public static void main(String[] args) {
		String pwd1 = "#abcd345abcdgde45";
		String pwd2 = "$#%$&(@_#@#$#$#";
		String pwd3 = "a23bcd";
		String pwd4 = "123bcd";
		String pwd5 = "a23bcd_vdkeder34";

		System.out.println(pwd1 + " is " + (isValidate(pwd1) ? "" : "not ") + "valid");
		System.out.println(pwd2 + " is " + (isValidate(pwd2) ? "" : "not ") + "valid");
		System.out.println(pwd3 + " is " + (isValidate(pwd3) ? "" : "not ") + "valid");
		System.out.println(pwd4 + " is " + (isValidate(pwd4) ? "" : "not ") + "valid");
		System.out.println(pwd5 + " is " + (isValidate(pwd5) ? "" : "not ") + "valid");
	}

	private static boolean isValidate(String p) {
		// modyfy this method.
		return false;
	}
}
