package ict102.lbt2;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

//Write a program that removes duplicates from an array.

//Output:
/*
arr: [A, T, Z, E, F, A, A, T, C, T, C, T, E, E, F, R, T, T, R, R, E, E]
list: [A, T, Z, E, F, C, R]

*/

public class STD_12001336 {
	public static void main(String[] args) {
		String[] arr = { "A", "T", "Z", "E", "F", "A", "A", "T", "C", "T", "C", "T", "E", "E", "F", "R", "T", "T", "R", "R", "E", "E" };
		List<String> list = new ArrayList<>();

		// your code here

		System.out.println("arr: " + Arrays.toString(arr));
		System.out.println("list: " + list);
	}
}
