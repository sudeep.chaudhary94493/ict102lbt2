package ict102.lbt2;

//Write a program that checks a password.
//The password should include at least 1 alphabet letter and 1 digit.

//Output:
/*
#abcd345abcdgde45 is valid
$#%$&(@_#@#$#$# is not valid
abcdef is not valid
123456 is not valid
a23bcd_vdkeder34 is valid

*/

public class STD_12001372 {
	public static void main(String[] args) {
		String pwd1 = "#abcd345abcdgde45";
		String pwd2 = "$#%$&(@_#@#$#$#";
		String pwd3 = "abcdef";
		String pwd4 = "123456";
		String pwd5 = "a23bcd_vdkeder34";

		System.out.println(pwd1 + " is " + (isValidate(pwd1) ? "" : "not ") + "valid");
		System.out.println(pwd2 + " is " + (isValidate(pwd2) ? "" : "not ") + "valid");
		System.out.println(pwd3 + " is " + (isValidate(pwd3) ? "" : "not ") + "valid");
		System.out.println(pwd4 + " is " + (isValidate(pwd4) ? "" : "not ") + "valid");
		System.out.println(pwd5 + " is " + (isValidate(pwd5) ? "" : "not ") + "valid");
	}

	private static boolean isValidate(String p) {
		// modyfy this method.
		return false;
	}
}
